const { mysql } = require("./config");
const _knex = require("knex");
const _bookshelf = require("bookshelf");
const uuid = require("bookshelf-uuid");
const paranoia = require("bookshelf-paranoia");
const cache = require("./lib/bookshelf/cache");
const pagination = require("./lib/bookshelf/pagination");
const visibility = require("./lib/bookshelf/visibility");

const getBookshelf = connection => {
  const knex = _knex(connection);
  const bookshelf = _bookshelf(knex);

  bookshelf.plugin(visibility);
  bookshelf.plugin(pagination);
  bookshelf.plugin(paranoia);
  bookshelf.plugin(uuid);
  bookshelf.plugin(cache);

  return bookshelf;
};

const getModels = connection => {
  const bookshelf = getBookshelf({
    client: "mysql",
    connection: connection
  });
  const Models = {
    baas: bookshelf.Model.extend({
      tableName: `${mysql.database}.baas`,
      hasTimestamps: true,
      softDelete: true
    }),
    auth: bookshelf.Model.extend({
      tableName: `${mysql.database}.auth`,
      hasTimestamps: true,
      softDelete: true
    }),
    file: bookshelf.Model.extend({
      tableName: `${mysql.database}.file`,
      hasTimestamps: true,
      softDelete: true
    }),
    global: bookshelf.Model.extend({
      tableName: `${mysql.database}.global`,
      hasTimestamps: true,
      softDelete: true
    }),
    class: bookshelf.Model.extend({
      tableName: `${mysql.database}.class`,
      hasTimestamps: true,
      softDelete: true,
      // 解密数据
      auth: function() {
        return this.belongsToMany(Models.auth, "auth_id").through(
          Models.classAuth,
          "class_id",
          "auth_id"
        );
      }
    }),
    classAuth: bookshelf.Model.extend({
      tableName: `${mysql.database}.class_auth`,
      hasTimestamps: true,
      softDelete: true
    }),
    classTable: bookshelf.Model.extend({
      tableName: `${mysql.database}.class_table`,
      hasTimestamps: true,
      softDelete: true,
      // 加密数据
      secret: function() {
        return this.belongsTo(Models.auth, "auth_id");
      }
    }),
    classField: bookshelf.Model.extend({
      tableName: `${mysql.database}.class_field`,
      hasTimestamps: true,
      softDelete: true
    }),
    classFunction: bookshelf.Model.extend({
      tableName: `${mysql.database}.class_function`,
      hasTimestamps: true,
      softDelete: true
    }),
    classHook: bookshelf.Model.extend({
      tableName: `${mysql.database}.class_hook`,
      hasTimestamps: true,
      softDelete: true
    }),
    classSchedule: bookshelf.Model.extend({
      tableName: `${mysql.database}.class_schedule`,
      hasTimestamps: true,
      softDelete: true
    }),
    table: bookshelf.Model.extend({
      tableName: `${mysql.database}.table`,
      hasTimestamps: true,
      softDelete: true
    }),
    relation: bookshelf.Model.extend({
      tableName: `${mysql.database}.relation`,
      hasTimestamps: true,
      softDelete: true
    }),
    log: {
      debug: bookshelf.Model.extend({
        tableName: `${mysql.database}_log.debug`,
        hasTimestamps: true,
        softDelete: true
      }),
      request: bookshelf.Model.extend({
        tableName: `${mysql.database}_log.request`,
        hasTimestamps: true,
        softDelete: true
      }),
      sms: bookshelf.Model.extend({
        tableName: `${mysql.database}_log.sms`,
        hasTimestamps: true,
        softDelete: true
      })
    },
    plugin: {
      sms: bookshelf.Model.extend({
        tableName: `${mysql.database}_plugin.sms`,
        hasTimestamps: true,
        softDelete: true
      }),
      captchaImg: bookshelf.Model.extend({
        tableName: `${mysql.database}_plugin.captcha_img`,
        hasTimestamps: true,
        softDelete: true
      }),
      payWexin: bookshelf.Model.extend({
        tableName: `${mysql.database}_plugin.pay_weixin`,
        hasTimestamps: true,
        softDelete: true
      }),
      payWxa: bookshelf.Model.extend({
        tableName: `${mysql.database}_plugin.pay_wxa`,
        hasTimestamps: true,
        softDelete: true
      }),
      payAlipay: bookshelf.Model.extend({
        tableName: `${mysql.database}_plugin.pay_alipay`,
        hasTimestamps: true,
        softDelete: true
      }),
      mail: bookshelf.Model.extend({
        tableName: `${mysql.database}_plugin.mail`,
        hasTimestamps: true,
        softDelete: true
      })
    }
  };
  return {
    Models,
    bookshelf
  };
};

module.exports = {
  getBookshelf,
  getModels
};
